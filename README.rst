``kairos-ceph``
===============

This repository contains tooling to extend stock [Kairos](https://kairos.io/) OS images
with kernel modules required to serve Ceph RBD and CephFS StorageClasses (as with
[Rook](https://rook.io/).)
