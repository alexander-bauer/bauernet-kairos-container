#!/bin/bash

repo="${1:-quay.io/kairos/opensuse}"
skopeo list-tags docker://quay.io/kairos/opensuse | \
  jq --arg "repo" "$repo" '.Tags | map(capture("^(?<flavor_release>[^-]+)-(?<variant>[^-]+)-(?<arch>[^-]+)-(?<device>[^-]+)-(?<version>.+)$") + {tag: ., repo: $repo})'
