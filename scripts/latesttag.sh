#!/bin/bash

repo="${1:-quay.io/kairos/opensuse}"; shift
flavor_release="${1:-tumbleweed}"; shift
variant="${1:-standard}"; shift
arch="${1:-amd64}"; shift
k3sv="${1:-k3sv1.29.0}"; shift

"$(dirname "$0")/taginfo.sh" "$repo" | jq \
  -r \
  --arg flavor_release "$flavor_release" \
  --arg variant "$variant" \
  --arg arch "$arch" \
  --arg k3sv "$k3sv" \
  'map(select(
    (.flavor_release == $flavor_release)
    and (.variant == $variant)
    and (.arch == $arch)
    and (.version | contains($k3sv))
    and (.version | contains("-rc") | not)
    and (.version | contains("alpha") | not)
  )) | max_by(.version)
  | "\(.repo):\(.tag)"
'
