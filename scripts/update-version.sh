#!/bin/bash
#
# This script updates the `base_version.txt` in the repository root.

repo="$1"; shift
repo_root="$(dirname "$0")/.."

mv -f "$repo_root/base_version.txt" "$repo_root/base_version.txt.prev"
"$repo_root/scripts/latesttag.sh" "$repo" > "$repo_root/base_version.txt"

diff "$repo_root/base_version.txt.prev" "$repo_root/base_version.txt"
